import { getUrl, setUrl } from "../../data/url";

export default function handler(req, res) {
    console.log(handler);
    if (req.method === 'GET') {
        res.status(200).json(getUrl());
    } else if (req.method === 'POST') {
        console.log("handler handling post")
        const { url } = req.body;
        setUrl(url);
        res.status(200).json("OK");
    }

}

