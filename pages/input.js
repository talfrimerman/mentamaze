import { useState } from "react"
import { setUrl } from "../data/url" ;

export default function Input() {

    const BASE_GOOGLE_SEARCH_URL = "https://www.google.com/search";

    const [input, setInput] = useState("");
    const submit = async () => {
        const url = new URL(BASE_GOOGLE_SEARCH_URL);
        url.searchParams.append('q', input)
        const urlStr = url.toString();
        const response = await fetch('/api/redirectLink', {
            method: 'POST',
            body: JSON.stringify({url : urlStr}),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const resData = await response.json();
        console.log(resData);
    }

    return <>
        <input 
            placeholder='enter name here'
            type='text'
            value={input}
            onChange={(e) => setInput(e.target.value)}
        />
        <button
            onClick={submit}
        >
            OK
        </button>
    </>
}