import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { useEffect } from 'react'

export default function Home() {
  const DELAY_BETWEEN_REQUESTS_MS = 3000;

  useEffect(() => {
    const lookForUrl = async () => {
      const res = await fetch ('/api/redirectLink');
      const linkStr = await res.json();
      if (linkStr) {
        // set url back to empty string to be ready for next time
        fetch('/api/redirectLink', {
          method: 'POST',
          body: JSON.stringify({url : ""}),
          headers: {
              'Content-Type': 'application/json'
          }
      })
        // redirect
        window.location.href = linkStr;
      } else {
        console.log("not found yet")
      }
    } 
    window.setInterval(lookForUrl, DELAY_BETWEEN_REQUESTS_MS);
  }, [])
  
  
  return (
    <div className={styles.container}>
        
      
    </div>
  )
}
